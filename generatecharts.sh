#! /bin/sh

# Plant UML files
java -jar plantuml.jar "./docs/charts/**.puml" -o "../assets"

# Dotfiles
find ./docs/charts/ -iname '*.dot' -exec bash -c 'dot -Tpng ${0} -o ${PWD}/docs/assets/$(basename ${0%%.dot}).png' {} \;
