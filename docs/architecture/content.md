---
id: content
title: Loading Content
---
![Some Diagram](assets/content.png)

1. Discovery: Get list of campaign projects either through
  * replaying the chain and filtering events
  * or loading from a directory (on or off-chain)
 
2. Iterate contracts and load
  * Project information (goals, supporterts, etc)
  * Content root URI (IFPS, HTTP, ...)

3. Process content root
  * Load additional assets (Images, TXT, HTML)

4. Verify safety and display
  * *Filter against malicious content* (JS, etc) before displaying! Remember that any content can be uploaded/linked by anyone who runs a project and there is no "server-side" check.


## Content root data structure

```
{
  version: string,
  card: {
    images: URI[],
    headline: string,
    content: string | URI
    website: URI
  }
...
}
```

The version field should only be used for extreme changes. Instead the abscence/presence of fields can be used to continously change the data structure.