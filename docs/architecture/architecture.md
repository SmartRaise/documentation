---
id: architecture
title: Architecture
---
## Overview

![Some Diagram](assets/architecture.png)


## Decentralization
All frontends can be started form a static local copy. All content can be stored for IPFS. 

For speedy access and ease of development, content can optionally also be loaded from static hosts like github, gitlab, etc

All campaign contracts work standalone. To discover campaign contracts, the chain can be searched for launch events. Optionally a directory service can be used, that is under the control of an entitiy and can provide additional trust, meta-information, etc. Also this provides resiliency against forks losing event information.


## Frontends
There are several frontends:

- *Consumer / Supporter*: This frontend is pretty, works without special software and protects against possible malicious content.
- *Project Admin*: Allows campaign managers to manage project (start, update, ask for payout, cancel)
- *Content Admin*: Allows campaign managers to update content and upload it to IPFS
- *Auditing Frontend*: Allows auditors to manage payouts


## Documents
There are two types of documents:

- *Documents:* (eg PDFs) that are attached to the project
- *Project information*: A JSON structure that points to project photos and content