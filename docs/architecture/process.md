---
id: process
title: How Smart Raise works
---
<span style="color:green">Campaign Manager</span>
<span style="color:blue">Auditor</span>
<span style="color:red">Supporters</span>

## Setup

1. The <span style="color:green">Campaign Manager</span> creates a campaing, with phases, description. He also nominates an <span style="color:blue">Auditor</span>. (The nomination is actually for an ethereum address, the verification of the owner happens off-project)

2. The <span style="color:green">Campaign Manager</span> starts the project.

3. <span style="color:red">Supporters</span> finance it.

## Successfull funding and payout

1. Everything goes according to plan. The <span style="color:green">Campaign Manager</span> provides his documentation to the auditor off-chain and requests as payout. This can contain confidential information which does not get published. 

2. The <span style="color:blue">Auditor</span> writes an audit report, uploads it, and adds the URI/Hash into the project contract.

3. The <span style="color:blue">Auditor</span> sets a new payout limit.

4. The <span style="color:green">Campaign Manager</span> pulls the payout from the contract

## Cancellation, Refunds

Cancellations can be triggered by the <span style="color:green">Campaign Manager</span>, <span style="color:blue">Auditor</span>, or a timeout. (missed funding goal, missed phase timeline)

1. Payins and payout are blocked

2. <span style="color:red">Supporters</span> can retrieve payment from the remaining funds proportionally to their payin.

## Q & A

### Who are the auditors?
We leave maximum flexibility here, to accommodate various situations. Fundamentally the campaign can nominate anybody they want. For transparency, the auditor has to be picked before the project accepts payments.

* If it's a bigger organization already working with independent auditors, they can nominate one of them. 
* Smaller organizations might just pick a lawyer, notary, or similar
* On some projects a trustworthy person from the community might be enough.

If you need help to find an auditor, write me a message, we might have suitable contacts to professional auditors who do pro-bono work.


### Why is the payout not decided by the supporter community?

*It's too easy to game:* Some of the projects on the platform have only a few dozen supporters, so any type of "random selection" or voting could be gamed.

*It's a lot more complicated:* Any system to try to mitigate for fraud risk and possible edge cases, would introduce a lot of complexity with little upside. Also, we'd have to deal with inactivity, disputes, malicious intent and so on. Creating a sophisticated voting system is not the goal of SmartRaise, but to create a simple system that's easy to use for a lot of projects and supporters.

*Average people from the supporter community lack expertise:* Do you know how to verify an invoice from a foreign countrty? Do you know how to look for fraudelant patterns?

Having said that, campaigns can nominate anybody they like. In some cases a trustworthy person from the community might just be the right auditor for the project.