---
id: design
title: Design
---
## Mobile Overview Page
![Mobile Overview Page](assets/concepts/mobile-main.png)

## Mobile Detail Page
![Mobile Detail Page](assets/concepts/mobile-detail.png)
