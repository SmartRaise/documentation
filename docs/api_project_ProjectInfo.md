---
id: project_ProjectInfo
title: ProjectInfo
---

<div class="contract-doc"><div class="contract"><h2 class="contract-header"><span class="contract-kind">contract</span> ProjectInfo</h2><div class="source">Source: <a href="git+ssh://git@gitlab.com/coder6421/SRCon1/blob/v1.0.0/contracts/project/ProjectInfo.sol" target="_blank">project/ProjectInfo.sol</a></div></div><div class="index"><h2>Index</h2><ul><li><a href="project_ProjectInfo.html#">fallback</a></li></ul></div><div class="reference"><h2>Reference</h2><div class="functions"><h3>Functions</h3><ul><li><div class="item function"><span id="fallback" class="anchor-marker"></span><h4 class="name">fallback</h4><div class="body"><code class="signature">function <strong></strong><span>() </span><span>public </span></code><hr/></div></div></li></ul></div></div></div>
