---
id: Smartraise
title: Smartraise
---

<div class="contract-doc"><div class="contract"><h2 class="contract-header"><span class="contract-kind">contract</span> Smartraise</h2><p class="base-contracts"><span>is</span> <a href="supporters_SupportableStatistics.html">SupportableStatistics</a><span>, </span><a href="supporters_Payin_Payinable.html">Payinable</a><span>, </span><a href="payout_Refund_Refundable.html">Refundable</a><span>, </span><a href="payout_PayoutApproval.html">PayoutApproval</a></p><div class="source">Source: <a href="git+ssh://git@gitlab.com/coder6421/SRCon1/blob/v1.0.0/contracts/Smartraise.sol" target="_blank">Smartraise.sol</a></div></div></div>
