---
id: phasen
title: Phasen
---
## Bedienungsoberfläche
![Mobile Overview Page](assets/userguide/phases.png)

## 1. Vertragsicon und Vertragsadresse
Wenn Sie Verträge editieren, wird zur Fehlervermeidung ein "Identicon" für den Vertrag angezeigt. Dieser wird direkt aus der Vertragsaddresse generiert und sollte sich nie ändern! Drucken Sie am besten das Identicon und die Vertrag-Adresse aus, um zukünftig die Identität des Vertrages leicht überprüfen zu können.

## 2. Unternavigation

Hier können sie verschiedene Funktionen auswählen, mit denen Sie den Vertrag bearbeiten. 

## 3. Projekt-Status
Hier können Sie den Projekt-Status ändern. Je nach Projekt-Phase stehen hier verschiedene Möglichkeiten zur Auswahl.

## Projektphasen
### 4. Phasen Verschieben
Mit dem Anklicken der blauen Blocks können Sie die Reihenfolge der Phasen ändern

### 5. Phasen Bezeichnung
Diese Bezeichnung wird Benutzern angezeigt

### 6. Betrag für die Phase
Zielbetrag für die jeweilige Phase

### 7. Betrag in EUR
Der Betrag wird schätzungsweise als Richtwert umgerechnet. Beachten Sie, dass der Wechselkurs und Gebühren von Ihrem Exchange abhängen, und sich auch im Zeitverlauf ändern können. (Der Kurs, der hier angezeigt wird, wird vom Exchange "Kraken" bezogen, und ohne Transaktionsgebühren angezeigt.)

### 8. Phasen-Zieldatum
Datum, wann die Phase abgeschlossen sein muss

### 9. Löschen (nicht sichtbar)
Wenn Sie mit der Maus über die Zeilen fahren, erscheint ein Mistkübel-Icon, mit dem Sie Phasen löschen können.

### 10. Phasen hinzufügen

### 11. Gesamtsumme

### 12. Phasen speichern
Mit diesem Knopf können Sie die Änderungen in das Netzwerk übertragen. Erst wenn die Transaktion vom Netzwerk durchgeführt wurde, sind die Änderungen auf dem Smart Contract wirksam. Beachen Sie das Phasen bei nach Aktivierung des Projekts nicht mehr geändert werden können.

