---
id: projektbeschreibung
title: Projektbeschreibung
---
## Bedienungsoberfläche
![Mobile Overview Page](assets/userguide/content.png)

## 1. Projektüberschrift

## 2. Projektbeschreibung
Hier können sie die Projektbeschreibung einfügen. Beachten Sie, dass zur Formatierung nur Fett-Schrift und Kursiv möglich sind. Sie können außerdem Hyperlinks einfügen

## 3. "Über uns"

## 4. Bilder
Es können Hyperlinks zu Bildern hinzugefügt werden. Diese werden auf Erreichbarkeit überprüft, und dann in der Bilderliste aufgenommen. Ist ein Bild nicht erreichbar, erhalten Sie eine Fehlermeldung.

Mit der Maus können Sie die Reihenfolge der Bilder ändern, indem Sie sie mit gedrückter Maustaste ziehen. Das erste Bild in der Reihenfolge wird automatisch als Haupt-Projektbild verwendet.

Entfernen können Sie Bilder, indem Sie auf das rote Minus-Symbol klicken, das über jedem Bild angezeigt wird. 

Beachten Sie, dass Änderungen erst dann auf der Kampagne wirksam werden, wenn Sie die neuen inhalte gespeichert haben, und die dazugehörige Transaktion vom Ethereum-Netzwerk verarbeitet wurde.
