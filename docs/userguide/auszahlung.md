---
id: auszahlung
title: Auszahlung
---
## Bedienungsoberfläche
![Mobile Overview Page](assets/userguide/payout.png)

## Auszahlung durchführen

Geben Sie den gewünschten Auszahlungsbetrag in ETH an, und drücken Sie "Auszahlen".

Die Auszahlung erfolgt immer auf das ETH Konto, von dem die Auszahlung angefordert wird. Damit wird vermieden, dass Beträge verloren gehen.

