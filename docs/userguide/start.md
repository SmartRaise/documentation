---
id: start
title: Projekt Anlegen
---
## Bedienungsoberfläche
![Mobile Overview Page](assets/userguide/transaction-manager.png)

## 1. Hauptnavigation
Die Hauptnavigation öffnet weitere Oberflächen, um Verträge zu verwalten

## 2. Kampagne Anlegen
Um eine neue Kampagne anzulegen, klicken Sie "Deploy a new Contract".

Es öffnet sich Ihr Ethereum Client (zB Metamask), in dem Sie die Erstellung des Smart Contracts bestätigen. Wenn Sie bereit sind länger zu warten, können Sie die "Gas Price" (Transaktionsgebühren) problemlos halbieren, und je nach Netzwerkbelastung eine Durchführung in 15-30 Minuten erwarten.

![Mobile Overview Page](assets/userguide/metamask.png)

## 3. Panel: Netzwerk-Status
Im Netzwerk-Panel sehen Sie immer den Verbindungsstatus zum Ethereum Netzwerk und Fehlermeldungen, falls eine Verbindung nicht möglich ist. Sie sollten Änderungen an Verträgen nur dann vornehmen, wenn Sie eine stabile Verbindung haben. Im Zweifel laden Sie die Seite einfach neu, und die tatsächlich aktuellen Werte werden aus dem Ethereum-Netzwerk geladen.

## 4. Transaction Logs
Jede Änderung an Kampagen / Verträgen muss an das Ethereum Netzwerk übertragen, und vom Netzwerk bestätigt werden. Die Bearbeitung dauert normalerweise weniger als eine Minute.
In den Transaktions-Logs sehen Sie den aktuellen Status Ihrer Änderungen. Auch die Ergebnise der Transaktionen werden dort bestätigt, dh Fehlermeldungen, oder auch die Adresse einer neuen Kampagne
